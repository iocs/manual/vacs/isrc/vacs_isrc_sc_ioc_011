#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_mks946_937b
#
require vac_ctrl_mks946_937b,4.5.2


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_mks946_937b_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: ISrc-010:Vac-VEVMC-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = ISrc-010:Vac-VEVMC-01100, BOARD_A_SERIAL_NUMBER = 1601141250, BOARD_B_SERIAL_NUMBER = 1511050734, IPADDR = isrc-moxa-mks.tn.esss.lu.se, PORT = 4001")

#
# Device: ISrc-010:Vac-VVMC-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_mfc_mks_gv50a.iocsh", "DEVICENAME = ISrc-010:Vac-VVMC-01100, CHANNEL = A1, CONTROLLERNAME = ISrc-010:Vac-VEVMC-01100")

#
# Device: ISrc-010:Vac-VGD-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgd.iocsh", "DEVICENAME = ISrc-010:Vac-VGD-01100, CHANNEL = B1, CONTROLLERNAME = ISrc-010:Vac-VEVMC-01100")

#
# IOC: VacS-ISrc:SC-IOC-011
# Load a possible IOC-custom .iocsh file
#
iocshLoad("$(E3_CMD_TOP)/iocsh/isrc.iocsh")
